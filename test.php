<!doctype html>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Test </title>
  <link href="css/style.css" type="text/css" rel="stylesheet">
  </head>
 <body>
      <form action="test-001.php" method="post" target="answer">
	  <h3>1. HTML - это аббревиатура от?</h3>
		<input type="radio" name="q1" value="a1">Hyperlinks and Text Markup Language<br/>
		<input type="radio" name="q1" value="a2">Home Tool Markup Language<br/>
		<input type="radio" name="q1" value="a3">Hyper Text Markup Language<br/>
		<br/>
	<h3>2. Выберите HTML тег для самого крупного заголовка</h3>
		<input type="radio" name="q2" value="a4">&lt;h6&gt;<br/>
		<input type="radio" name="q2" value="a5">&lt;h1&gt;<br/>
		<input type="radio" name="q2" value="a6">&lt;head&gt;<br/>
		<br/>
	<h3>3. Выберите правильный вариант установки цвета фона страницы?</h3>
		<input type="radio" name="q3" value="a7">&lt;body background=&quot;red&quot;&gt;<br/>
		<input type="radio" name="q3" value="a8">&lt;background&gt;red&lt;/background&gt;<br/>
		<input type="radio" name="q3" value="a9">&lt;body style=&quot;background-color:red;&quot;&gt;<br/>
<br/>
        <h3>4. Выберите правильный способ создания ссылки?</h3>
		<input type="radio" name="q4" value="a10">&lt;a&gt;http://www.google.com&lt;/a&gt;<br/>
		<input type="radio" name="q4" value="a11">&lt;a url=&quot;http://www.google.com&quot;&gt;google.com&lt;/a&gt;<br/>
		<input type="radio" name="q4" value="a12">&lt;a href=&quot;http://www.google.com&quot;&gt;Google&lt;/a&gt;<br/>

<br/>
        <h3>5. В HTML строчные элементы отображаются нормально без перехода на новую строку</h3>
		<input type="radio" name="q5" value="a13">Истина<br/>
		<input type="radio" name="q5" value="a14">Ложь<br/>
	<br/>
<h3>6. Выберите верный способ вставки изображения</h3>
		<input type="radio" name="q6" value="a15">&lt;img alt=&quot;MyImage&quot;&gt;image.gif&lt;/img&gt;<br/>
		<input type="radio" name="q6" value="a16">&lt;img src=&quot;image.gif&quot;&gt;<br/>
                <input type="radio" name="q6" value="a17">&lt;img href=&quot;image.gif&quot;&gt;<br/>
	<br/>
<h3>7. Выберите верный способ выравнивания текста внутри абзаца</h3>
		<input type="radio" name="q7" value="a18">&lt;p style=&quot;center&quot;&gt;Текст&lt;/p&gt;<br/>
		<input type="radio" name="q7" value="a19">&lt;p style=&quot;align:center&quot;&gt;Текст&lt;/p&gt;<br/>
                <input type="radio" name="q7" value="a20">&lt;p style=&quot;text-align:center;&quot;&gt;Текст&lt;/p&gt;<br/>
	<br/>
<h3>8. Каким способом можно задать цвет заголовку?</h3>
		<input type="radio" name="q8" value="a21">&lt;title style=&quot;color:#00f;&quot;&gt;Заголовок&lt;/title&gt;<br/>
		<input type="radio" name="q8" value="a22">&lt;h3 color=&quot;blue&quot;&gt;Заголовок&lt;/h3&gt;<br/>
                <input type="radio" name="q8" value="a23">&lt;h2 style=&quot;color:#0000ff;&quot;&gt;Заголовок&lt;/h2&gt;<br/>
	<br/>

	<input type="submit" name="submit"value="Результат">
	  </form>
<iframe name="answer"style="width:500px;height:300px;border:1px solid">

</iframe>
 </body>
</html>